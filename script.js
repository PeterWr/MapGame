var cities;
var gameStart = false;
var score = 1500;
var citiesFound = 0;
var pinPlaced = false;
var randomCity;
(function($) {

  $.getJSON('capitalCities.json', function(data) {


    cities = data.capitalCities.map(function(item) {

      return item;
    });

  });

  $.widget("wgm.imgNotes2", $.wgm.imgViewer2, {
    options: {
      /*
       *	Default action for addNote callback
       */
      addNote: function(data) {


        var map = this.map,

          loc = this.relposToLatLng(data.x, data.y);
        loc2 = this.relposToImage({
          x: data.x,
          y: data.y
        });

        var marker = L.marker(loc).addTo(map).bindPopup(data.note + "</br><input type='button' value='Delete' class='marker-delete-button'/>");
        marker.on("popupopen", function() {
          var temp = this;
          $(".marker-delete-button:visible").click(function() {
            temp.remove();
          });
        });

        var latlngs = [
          [cities[randomCity].lat, cities[randomCity].long],
          [loc2.x, loc2.y]
        ];

        // Latitude Longtitude Variables extracted from JSON file
        var x1 = cities[randomCity].lat;
        var y1 = cities[randomCity].long;
        var x2 = loc2.x;
        var y2 = loc2.y;

        //Calculating Distance with Pythagoras Formula
        var width = x2 - x1;

        var height = y2 - y1;

        var widthPlusHeight = width * width + height * height;

        var pythagorasFormula = Math.sqrt(widthPlusHeight);
        var points = 0;
        var distanceInKm = pythagorasFormula * 4.8;
        distanceInKm = Math.round(distanceInKm)


        var scoreId = document.getElementById("score");
        var highscore = document.getElementById("highscore");


        if (distanceInKm < 50) {

          points++;
          score = score - distanceInKm;

          scoreId.textContent = "Points Left: " + score;

          citiesFound++;
          highscore.textContent = "Cities Found: " + citiesFound;
        } else {


          score = score - distanceInKm;
          scoreId.textContent = "Points left: " + score;



          if (score < 0) {
            $('#nextRound').prop('disabled', true);
            alert("Game Over!");
            // $("#nextRound").button({
            //   disabled: true
            // });
            //  document.getElementById("nextRound").disabled = true;
          }
        }

        if (gameStart = true) {
          $("#gameStart").removeClass("disabledbutton");
          var div = document.getElementById("textDiv");

          div.textContent = distanceInKm.toString() + " Kilometers Left";
          var text = div.textContent;
        }

        //Drawing PolyLine
        if (pinPlaced == false) {
          var polyline = L.polyline(latlngs, {
            color: 'red'
          }).addTo(map);
          pinPlaced = true;
        }

        // zoom the map to the polyline

        //	map.fitBounds(polyline.getBounds());

        // Function for removing Leaflet Elements from map, marker and polyline
        function removeMapLayers() {
          map.removeLayer(polyline);
          map.removeLayer(marker);
        }

        $("#nextRound").click(function() {
          pinPlaced = false;
          removeMapLayers();
          //Random City from JSON File
          var locationOfElement = document.getElementById("location");
          randomCity = Math.floor((Math.random() * 9) + 1);
          locationOfElement.textContent = "Select the Location of " + cities[randomCity].capitalCity;

        });


      }
    },

    /*
     *	Add notes from a javascript array
     */
    import: function(notes) {
      if (this.ready) {
        var self = this;
        $.each(notes, function() {
          self.options.addNote.call(self, this);
        });
      }
    }
  });
  $(document).ready(function() {

    //Game Start
    $("#gameStartButton").click(function() {

      if (gameStart == true)

      {
        location.reload(true);
      }

      var locationOfElement = document.getElementById("location");

      randomCity = Math.floor((Math.random() * 9) + 1);

      locationOfElement.textContent = "Select the Location of " + cities[randomCity].capitalCity;

      gameStart = true;

      scoreId = document.getElementById("score");
      highscore = document.getElementById("highscore");

      score = 1500;
      citiesFound = 0;

      scoreId.textContent = "Points Left: " + score;

      highscore.textContent = "Cities Found: " + citiesFound;

    });





    var $img = $("#image1").imgNotes2({
      onReady: function() {
        $('.leaflet-grab').css('cursor', 'crosshair');
      },
      onClick: function(e, pos) {


        // Coordinantes System
        // var $message = $("<div id='dialog-modal'></div>").dialog({
        // 					modal: true,
        // 					title: "You clicked at:",
        // 					open: function(ev, ui) {
        // 						$('.ui-dialog').css('z-index',2001);
        // 						$('.ui-widget-overlay').css('z-index',2000);
        // 					}
        // });
        var imgpos = this.relposToImage(pos);

        //Showing Coordinantes System on Map
        //Commented out for Debug purposes

        //$message.html("Page X: " + e.pageX + "<br/>Page Y: " + e.pageY + "<br/>Image Pixel X: " + imgpos.x + "<br/>Image Pixel Y: " + imgpos.y + "<br/>Image Rel X: " + pos.x.toFixed(3) + "<br/>Image Rel Y: " + pos.y.toFixed(3));
        if (gameStart == true && pinPlaced == false) {
          var notes = [


            {
              x: pos.x.toFixed(3),
              y: pos.y.toFixed(3).toString()
            }
          ];
          this.import(notes);

        }


      }
    });
  });
})(jQuery);
